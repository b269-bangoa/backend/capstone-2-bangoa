const Product = require("../models/Product");


//Add product (Admin only)
module.exports.registerProduct = (data) => {
	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price,
			SKU: data.product.SKU,
			stock: data.product.stock
		});
		return newProduct.save().then((user, err) => {
			if(err) {
				return false;
			} else {
				return true;
			}
		});
	} else {
		return Promise.resolve("Warning! No authority to register products.");
	}
};

module.exports.checkProductExists = (reqBody) => {
	return Product.findOne({name: reqBody.name}).then(result => {
		console.log(result)
		if (result === null){
			return false;
		} else {
			return true;
		}
	})

}

//Show active products
module.exports.showProducts = () => {
	return Product.find({isActive : true}).then(result => {
		console.log(result);
		return result;
	});
};

//Retrieve single product
module.exports.getSingleProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result =>{
		return result;
	})
};

//Update Product info (Admin only)
module.exports.updateProduct = (data) => {
  if (data.isAdmin) {
    const updateProduct = {
      name: data.product.name,
      description: data.product.description,
      price: data.product.price,
      SKU: data.product.SKU,
      stock: data.product.stock,
      isActive: data.product.isActive
    };

    return Product.findByIdAndUpdate(data.productId, updateProduct, {new: true})
      .then((product, err) => {
        if (err) {
          return false;
        } else {
          return product;
        }
      });
  } else {
    return Promise.resolve("Warning! Unauthorized to update product.");
  }
};

//Archieve product (Admin only)
module.exports.archiveProduct = (data) => {
  if (data.isAdmin) {
    const updateState = { isActive: false }; 
    return Product.findByIdAndUpdate(data.productId, updateState)
      .then((product) => {
        if (product.isActive) {
          product.isActive = false;
        } else {
          product.isActive = true;
        }
        return product.save();
      })
      .catch((err) => {
        console.error(err);
      });
  }
};


//Retrieve All products (admin only)
module.exports.showAllProducts = (isAdmin) => {
	if(isAdmin){
		return Product.find().then(result => {
			return result;
			});
	} else {
		return false;
	}
};