const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//User registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		username: reqBody.username,
		password: bcrypt.hashSync(reqBody.password, 10),
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
	});
	return newUser.save().then((user, err) => {
		if(err) {
			return false;
		} else {
			return true;
		}
	});
			
};

module.exports.checkEmailExists = async (reqBody) => {
	return await User.find({email : reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};


//User access token (Admin only)
module.exports.userToken = (data) => {
	if(data.isAdmin){
		return User.findOne({ username: data.user.username}).then(result => {
			if(result == null){
				return false;
			} else{
				const isPasswordCorrect = bcrypt.compareSync(data.user.password, result.password);
				if(isPasswordCorrect) {
					return { access: auth.createAccessToken(result)}
				} else {
					return false
				}
			}
		});
	} else {
		return res.send({auth: "failed"});
	}
	
};

//Login User
module.exports.loginUser = (reqBody) => {
	return User.findOne({username: reqBody.username}).then(result =>{
		if (result == null){
			return false
		  // User exists
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};

// Show Registered Users (Admin Only)
module.exports.showUsers = (isAdmin) => {
	if(isAdmin) {
		return User.find().then(result => {
				return result;
			});
	} else {
		return Promise.resolve("Warning! No authority to check users.");
	}
};

//Retrieve User detail
module.exports.getProfile = (data) => {

	return User.findById(data.id).then(result => {
		
		result.password = "";
		return result;
	});
};

//Set User as Admin
module.exports.setUserAsAdmin = (data) => {
  if (data.isAdmin) {

    const updateState = { isAdmin: false }; 

    return User.findByIdAndUpdate(data.userId, updateState)
      .then((user) => {
        if (user.isAdmin) {
          user.isAdmin = false;
        } else {
          user.isAdmin = true;
        }
        return user.save();
      })
      .catch((err) => {
        console.error(err);
      });
  }
};

//Add to Cart function
module.exports.addToCart = async (data) => {

	const item = data.item;
	const userId = data.userId;
	const productId = item.productId;
			const quantity = item.quantity;

	//Add to cart feature for non-admin only
	if (!data.isAdmin) {
		//Retrieve Product details
		return await Product.findById(productId).then((product) => {
			if (!product) {
				return false;
			}

			//Define data
			const price = product.price;
			const subTotal = price*quantity;

			//Check stock vs user-quantity

			if(product.stock >= quantity) {
				return User.findById(userId).then(user => {
					//Check for duplicate item
					const itemIndex = user.cart.products.findIndex((product) => product._id.toString() === productId);

					//Product exist in user cart
					if(itemIndex >= 0) {
						user.cart.products[itemIndex].quantity = quantity;
						user.cart.products[itemIndex].subTotal = subTotal;
					} else {
						//Push products in user cart
						user.cart.products.push({
							productId: productId,
							quantity: quantity,
							subTotal: subTotal
						})

						return user.save().then((user, err) => 
						{
							if (err) 
							{
								return false;
							} 
							else 
							{
								return true; 
							}
						})
					}
				})
			//Quantity exceeds stock
			} else {
				return Promise.resolve("Insufficient stock");
			}
		})
	} else {
		return ("Admin can't use this feature");
	}
};

//Show all Carts aside from admin
module.exports.showCarts = (isAdmin) => {
  if (isAdmin) {
    return 'User must NOT be an admin to access this!';
  }
  return User.find({ isAdmin: false }).then((users, err) => {
    if (err) {
      return `Error retrieving carts: ${err}`;
    } else {
      const carts = users.map(user => user.cart);
      return carts;
    }
  });
};

//Show specific user cart
module.exports.showUserCart = async (userData) => {
  try {
    const user = await User.findOne({ userId: userData.id }).exec();
    console.log(user.cart.products)
    return user.cart.products;
  } catch (error) {
    console.log(error);
  }
};




