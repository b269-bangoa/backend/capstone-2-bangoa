const Order = require("../models/Order");
const User = require("../models/User");

module.exports.createOrder = (userData, productId, quantity, total) => {
  if (!userData.isAdmin) {
    let newOrder = new Order({
      userId: userData.id,
      productId: productId,
      quantity: quantity,
      total: total
    });
    return newOrder.save().then((order, err) => {
      if (err) {
        return false;
      } else {
        return order;
      }
    });
  } else {
    return Promise.reject(new Error("Warning! No authority to register products."));
  }
};

module.exports.getAllOrders = () => {
	return Order.find().then((orders, err) => {
		if(err){
			return `Error retrieving orders: ${err}`;
		} else {
			return orders;
		}
	});
};

//get single order
module.exports.getSingleOrder = (reqParams) => {
	return Product.findById(reqParams.orderId).then(result =>{
		return result;
	})
};

//Change status of order (admin only)
module.exports.setStatus = (data) => {
  if (data.isAdmin) {
    const updateState = { isActive: false }; 
    return Order.findByIdAndUpdate(data.orderId, updateState)
      .then((order) => {
        if (order.isActive) {
          order.isActive = false;
        } else {
          order.isActive = true;
        }
        return order.save();
      })
      .catch((err) => {
        console.error(err);
      });
  }
};

module.exports.getUserOrders = (userId) => {
	return Order.find({userId: userId}).then(result => {
		return result;
	})
}

// Extra functions not used
//Retrieve active orders (admin only)
/*module.exports.getActiveOrders = async (userData) => {

	return await User.find().then(orders => {
		let total = userData.cart.reduce((total, acc) => total = total + acc.subTotal, 0);
		if(userData.isAdmin) {
			let orderCart = new Order ({
				userId: userData.id,
				products: userData.products,
				total: total
			})
			return newUser.save().then((user, err) => {
				if(err) {
					return false;
				} else {
					return true;
				}
			})
		} else {
			return ("No authority to check orders")
		}
	})	
};*/

//To be shown as greyed out in orders page
/*module.exports.getInactiveOrders = async (isAdmin) => {
	if(isAdmin) {
		return await User.find({"cart.isActive": false}, {firstName: 0, username: 0, lastName: 0, email: 0, mobileNo: 0, isAdmin: 0, password:0}).then(orders => {
			return orders;
		});
	} else {
		return ("No authority to check orders")
	}	
};

//Check order details
module.exports.checkOrderDetails = async (isAdmin, reqParams) => {
	if(isAdmin) {
		return await User.findById({userId: reqParams.userId} , {username: 0, password: 0, isAdmin: 0}).then(order => {
			return order;
		});
	} else {
		return ("No authority to check orders");
	}	
};*/