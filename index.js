//Depencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute")

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute);

mongoose.connect ("mongodb+srv://yanab:admin123@zuitt-bottcamp.muceymo.mongodb.net/ecommerceAPI?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
});

mongoose.connection.once("open", () => console.log(`Now connected to cloud database`));


app.listen(process.env.PORT || 3005, () => console.log(`Now connected to ${process.env.PORT||3005}`));