const mongoose = require("mongoose");
const User = require("../models/User");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String
	},
	productId: {
		type: String
	},
	quantity: {
		type: Number,
		default: 1
	},
	total: {
		type: Number
	},
	isActive: {
		type: Boolean,
		default: true
	},
	purchasedOn: {
		type: Date,
		default: new Date()}
	}
);

module.exports = mongoose.model("Order", orderSchema);