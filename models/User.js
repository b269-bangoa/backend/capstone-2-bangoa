const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: [true, "Valid username is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	cart : {
		userId: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'User'
		},
		products: [
			{
				productId: {
					type: String
				},
				quantity: {
					type: Number,
					required: true,
					default: 1
				},
				subtotal: {
					type: Number
				}
			}
		],
		total: {
			type: Number
		},
		isActive: {
			type: Boolean,
			default: true
		},
		purchasedOn: {
			type: Date,
			default: new Date()}

	}
});

module.exports = mongoose.model("User", userSchema);

