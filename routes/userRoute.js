const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userController");

router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/all", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	userController.showUsers(isAdmin).then(resultFromController => res.send(resultFromController));
});

router.post("/auth", auth.verify, (req, res) => {

	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		user: req.body
	}

	userController.userToken(data).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});

router.patch("/:userId/set-as-admin", auth.verify, (req, res) => {

		const data = {
			isAdmin: auth.decode(req.headers.authorization).isAdmin,
			userId: req.params.userId 
		}

	userController.setUserAsAdmin(data).then(resultFromController => res.send(resultFromController));
});

router.post("/:userId/cart/add", auth.verify, (req, res) => {
	const data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		item: req.body
	};

	userController.addToCart(data).then(resultFromController => res.send(resultFromController));
});

router.get("/carts/show", auth.verify, (req, res) => {

  const isAdmin = auth.decode(req.headers.authorization).isAdmin;

  userController.showCarts(isAdmin).then(resultFromController => res.send(resultFromController));
});

router.get("/cart/:userId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.showUserCart({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});


module.exports = router;