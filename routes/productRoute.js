const express = require("express");
const router = express.Router();
const auth = require("../auth");

const productController = require("../controllers/productController");

router.post("/add", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		product: req.body 
	}

	productController.registerProduct(data).then(resultFromController => res.send(resultFromController));
});

router.post("/checkProduct", (req,res) => {
	productController.checkProductExists(req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/active", (req,res) => {
	productController.showProducts().then(resultFromController => res.send(resultFromController));
});


router.get("/:productId", (req, res) => {
	productController.getSingleProduct(req.params).then(resultFromController => res.send(resultFromController));
});

router.patch("/:productId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.params.productId,
		product: req.body
	}
	productController.updateProduct(data).then(resultFromController => res.send(resultFromController));
});

router.patch("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.params.productId 
	}
	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController));
});


router.post("/all", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	
	productController.showAllProducts(isAdmin).then(resultFromController => res.send(resultFromController));
});

module.exports = router;