const express = require("express");
const router = express.Router();
const auth = require("../auth")

const orderController = require("../controllers/orderController");

router.post("/add", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  const { productId, quantity, total } = req.body;
  orderController.createOrder(userData, productId, quantity, total)
    .then(resultFromController => res.send(resultFromController))
    .catch(err => res.status(500).send(err.message));
});

router.get("/all", auth.verify, (req, res) => {

	orderController.getAllOrders().then(resultFromController => res.send(resultFromController));
});

router.get("/:userId", auth.verify, (req, res) => {
	isAdmin = auth.decode(req.headers.authorization).isAdmin;

	orderController.checkOrderDetails(req.params, isAdmin).then(resultFromController => res.send(resultFromController))
	;
});

router.patch("/:orderId/status", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		orderId: req.params.orderId 
	}
	orderController.setStatus(data).then(resultFromController => res.send(resultFromController));
});

router.get("/:orderId", (req, res) => {
	orderController.getSingleOrder(req.params).then(resultFromController => res.send(resultFromController));
});

router.get("/:userId/orders", auth.verify, (req, res) => {

	const userId = auth.decode(req.headers.authorization).id;
	orderController.getUserOrders(userId).then(resultFromController => res.send(resultFromController));
});

module.exports = router;